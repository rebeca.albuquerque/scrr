class CreateReservations < ActiveRecord::Migration[6.0]
  def change
    create_table :reservations do |t|
      t.boolean :active
      t.references :client, null: false, foreign_key: true
      t.references :book, null: false, foreign_key: true
      t.references :librarian, null: false, foreign_key: true

      t.timestamps
    end
  end
end
