class Reservation < ApplicationRecord
  belongs_to :client
  belongs_to :book
  belongs_to :librarian
end
